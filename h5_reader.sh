#!/usr/bin/env bash

echo -n "Enter date(Ex: 2018/09/04): "
#read date

#unzip -j "/home/picarro/SI200/Log/Archive/DataLog_Private/${date}/*.zip" -d "h5_files"
unzip -j "/Users/kenny/Documents/Work Projects/h5_reader/h5/*.zip" -d "h5_files"

python "/Users/kenny/Documents/Work Projects/h5_reader/h5_reader.py"
wait

rm -rf "h5_files"
echo "Done!"
