import os
import h5py
import numpy as np
from datetime import datetime


def main():
    list_of_filenames = os.listdir("/Users/kenny/Documents/Work Projects/h5_reader/h5_files/")
    new_list_of_filenames = [files for files in list_of_filenames if ".h5" in files]

    print new_list_of_filenames
    f = open("h5_reader_output.txt", "w")

    list_of_interval = []
    list_of_time = []

    for filename in new_list_of_filenames:
        relative_filename = "h5_files/" + filename
        h5_file = h5py.File(relative_filename, 'r')
        results = h5_file['results']

        try:
            interval_arr = np.array(results['interval'])
            time_arr = np.array(results['time'])
            list_of_interval.extend(interval_arr)
            list_of_time.extend(time_arr)
        except ValueError:
            f.write("Error: File '%s' does not contain correct columns\n" % filename)
            print("Error: File '%s' does not contain correct columns\n" % filename)

        h5_file.close()

    total = 0
    count = 0
    average = 0

    for value in list_of_interval:
        time = str(datetime.fromtimestamp(list_of_time[count]))
        total += value
        count += 1
        average = (total / float(count))

        if value > (average + 2.0):
            f.write("Spike in data at time: %s, with value: %f\n" % (time, value))
            print("Spike in data at time: %s, with value: %f\n" % (time, value))

    f.write("Average interval value: %f\n" % average)
    print "Average interval value: %f\n" % average

    f.close()


if __name__ == "__main__":
    main()
